import { Injectable } from '@angular/core'
import * as io from 'socket.io-client'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs/Observable'

@Injectable({
  providedIn: 'root'
})
export class SocketsService {
  public url = 'https://minime.serveo.net'
  public socket      
  constructor(private http: HttpClient) {
    this.socket = io(this.url)
    
    console.log(this.socket)
  }
 public  sendMessage(req, userid) {
    console.log(this.socket.id,"sent to "+userid)
    this.socket.emit('sendReq', req, userid)
  }
  public  endchat(userid) {
    console.log(userid+" chat ended")
    this.socket.emit('end', userid)
  }
  public  sendack(userid, sid) {
    console.log(sid +' sendack')
    this.socket.emit('ack', userid, sid)
  }
  public  getreq(req) {
    console.log(this.socket.id+'sendack')
    this.socket.emit('req', 'requests')
  }
  public reqs = () => {
    return Observable.create((observer) => {
      this.socket.on('reqw', (reqs) => {        
        console.log('requests',reqs)
        observer.next(reqs)
      })
    })
  }

  public acp = () => {
    return Observable.create((observer) => {
      this.socket.on('acp', (acp) => {
        console.log('accepted',acp)
        observer.next(acp)
      })
    })
  }

  public getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('message', (message, fbuser ) => {
        console.log('message', message, fbuser)
        observer.next([fbuser, message ] )
      })
    })
  }
// public getrequest() {
//   return fetch('https://fafc91d6.ngrok.io/getrequests')
//         .then(function(response) {
//            return response.json()
//         }) .then(function(data) {
//                 console.log(data+'getrequest')
//                 return data

//             })
// }

  }


