import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Component, ElementRef, Pipe, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core'
import { HttpClient,  } from '@angular/common/http'
import { SocketsService } from '../sockets.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  log = false;
  constructor(public route: Router,public service: SocketsService, public http: HttpClient) { 
    localStorage.clear()
  }
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  pwd = new FormControl('', [
    Validators.required,
  ]);
  onClickSubmit(v) {
    if (this.emailFormControl.value == "admin@miraclesoft.com" && this.pwd.value == "admin") {
      this.route.navigateByUrl('/chat')
    } else {
      this.log = true;
    }
  }
  encapsulation: ViewEncapsulation.None
@Input() public buttonText = '↩︎'
@Input() public focus = new EventEmitter()
@Output() public send = new EventEmitter()
@Output() public dismiss = new EventEmitter()
@ViewChild('message',{static: false}) message: ElementRef
@ViewChild('bottom',{static: false}) bottom: ElementRef
 
persons = []
person = [{"_id":{"$numberDouble":"2172391996216421"},"messages":[{"type":"receive","message":"hi","timestamp":{"$numberDouble":"1562751880403"}},{"type":"sent","message":"I'm Amelia. I can help you to answer any question that you might have about our company","timestamp":{"$numberDouble":"1562751883125"}},{"type":"receive","message":"What can you do","timestamp":{"$numberDouble":"1562751901082"}},{"type":"sent","message":"My name is Amelia and I was built by Miracle’s Innovation Labs! I can help with some of the following about Miracle, Job Search, Internship, Something else","timestamp":{"$numberDouble":"1562751903809"}},{"type":"receive","message":"Digital summit","timestamp":{"$numberDouble":"1562752008699"}},{"type":"sent","message":"Digital Summit is a 5-day technical extravaganza that takes place in Vizag every year.","timestamp":{"$numberDouble":"1562752010360"}}],"status":"none","first_name":"Yagnesh","last_name":"Tendulkar","profile_pic":"https://platform-lookaside.fbsbx.com/platform/profilepic/?psid=2172391996216421&width=1024&ext=1564681963&hash=AeRfWOr1fdKI9_yF","__v":{"$numberInt":"0"}}]
agentStatus = false
userimg
fbuser
sid
display=false
psn=false
dname
dpic
public y: boolean = false;
public dat
public messages = []
public addMessage(from, text, type: 'received' | 'sent',date) {
   
  this.scrollToBottom() 
  this.messages.unshift({
      from,
      text,
      type,
      date,
    })
    this.scrollToBottom()
   
  }
public scrollToBottom() {
  // this.ngxAutoScroll.forceScrollDown();  
  if (this.bottom !== undefined) {
      this.bottom.nativeElement.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"})
    }
  }
userlog(data) {  
  
  localStorage.clear()
this.display=true;
  this.dname=data.first_name;
  this.dpic=data.profile_pic;
  this.service.sendack(data.id, this.sid )
  localStorage.setItem('id',data.id)
  console.log(data)
  this.http.post('https://bot.serveo.net/getrequests', { '_id' : data.id } ).subscribe(res => {
    console.log(res)
  this.dat = res
   this.messages = []
console.log(this.dat.messages)
console.log(this.dat)
this.dat.messages.forEach(element => {
this.addMessage('data.first_name', element.message, element.type,element.timestamp)
})
})
}


public sendMessage({ message }) {
console.log(this.messages)
    this.service.sendMessage(message, localStorage.getItem('id'))
    if (message.trim() === '') {
      return
    }
    this.addMessage('this.client', message, 'sent',new Date().getTime())
  }


end() {
  this.messages = []
  this.service.endchat(localStorage.getItem('id'))
  this.display=false
  this.persons = []
  console.log(localStorage.getItem('id'))
   this.http.post('https://bot.serveo.net/update', { '_id' : localStorage.getItem('id'), 'status': 'none' } ).subscribe(res => {
    console.log("res")
    this.messages = []
  })
  

  // setTimeout(() => {
  // this.service.getrequest().then(data => {
  //   console.log(data.length)
  //   this.persons = data
  //   console.log('data inside end()' + data)
  //   })
  //   }, 2000)

    localStorage.clear()
}
fun(d) {
  console.log(this.service.socket.id)
  if (!d) {
  this.agentStatus = true
  } else {
  alert('now you are offline')
  this.agentStatus = false
  }
}

ngOnInit() {
  if(this.persons.length==0){
    this.psn=true
  }
  this.agentStatus = true
  this.service.getreq('msg')
  this.service.reqs()
    .subscribe((reqs) => {
      this.sid = this.service.socket.id
      console.log(this.sid)
      this.persons = []
      reqs.forEach(element => {
      if ((element.skt === '' || element.skt === this.sid) &&  element.status !== 'none') {
        this.persons.push(element)
      }
      })
      console.log(this.persons)
    })
    this.service.acp()
    .subscribe((acp) => {
      console.log(acp, this.persons)
      console.log(this.persons.findIndex(x => x.id === acp))
    })

  
//    this.service.getrequest().then(data => {
//     this.persons = data
//     })

  
    this.service.getMessages()  
    .subscribe((message: string) => {
      console.log('getmessages')    
      console.log(message)
      if (message[0] === localStorage.getItem('id') ) {
      this.addMessage('me', message[1], 'received',new Date().getTime())
      }   
      console.log('message recived', message)
})
}

}
