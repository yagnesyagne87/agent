import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations'
import { Component, ElementRef, Pipe, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core'
import { Subject } from 'rxjs'
import { HttpClient,  } from '@angular/common/http'
import { SocketsService } from '../sockets.service'
import {NgxAutoScroll} from "ngx-auto-scroll";
import { HostListener } from "@angular/core";
import * as io from 'socket.io-client'
import { getLocaleDayNames } from '@angular/common';
@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
encapsulation: ViewEncapsulation.None
@Input() public buttonText = '↩︎'
@Input() public focus = new EventEmitter()
@Output() public send = new EventEmitter()
@Output() public dismiss = new EventEmitter()
@ViewChild('message',{static: false}) message: ElementRef
@ViewChild('bottom',{static: false}) bottom: ElementRef
@HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
  event.returnValue = false;
  }
  
  
  constructor(public service: SocketsService, public http: HttpClient) {     
  }
persons = []
agentStatus = false
userimg
fbuser
sid
mes=["a",'b']
display=false
psn=false
dname
dpic
public y: boolean = false;
public dat
public messages = []
public addMessage(from, text, type: 'received' | 'sent',date) {
   
  this.scrollToBottom() 
  this.messages.unshift({
      from,
      text,
      type,
      date,
    })
    this.scrollToBottom()
   
  }
public scrollToBottom() {
  // this.ngxAutoScroll.forceScrollDown();  
  if (this.bottom !== undefined) {
      this.bottom.nativeElement.scrollIntoView({behavior: "auto", block: "end", inline: "end"})
    }
  }
userlog(data) {  
localStorage.clear()
this.display=true;
data.status="accepted"
  this.dname=data.first_name;
  this.dpic=data.profile_pic;
  this.service.sendack(data.id, this.sid )
  localStorage.setItem('id',data.id)
  console.log(data)
  this.http.post('https://minime.serveo.net/getrequests', { '_id' : data.id } ).subscribe(res => {
    console.log(res)
  this.dat = res
   this.messages = []
console.log(this.dat.messages)
console.log(this.dat)
this.dat.messages.forEach(element => {
this.addMessage('data.first_name', element.message, element.type,element.timestamp)
})
})
}


public sendMessage({ message }) {
  console.log(this.bottom)
console.log(this.messages)
    this.service.sendMessage(message, localStorage.getItem('id'))
    if (message.trim() === '') {
      return
    }
    this.addMessage('this.client', message, 'sent',new Date().getTime())
  }


end() {
  this.messages = []
  this.display=false
  this.persons = []
  console.log(localStorage.getItem('id'))
  this.service.endchat(localStorage.getItem('id'))
   this.http.post('https://minime.serveo.net/update', { '_id' : localStorage.getItem('id'), 'status': 'none' } ).subscribe(res => {
    console.log("res")

  })
    localStorage.clear()
    this.messages = []
    this.dat=[]
}
fun(d) {
  console.log(this.service.socket.id)
  if (!d) {
  this.agentStatus = true
  } else {
  alert('now you are offline')
  this.agentStatus = false
  }
}
ngOnInit() {
  localStorage.clear()
  if(this.persons.length==0){
    this.psn=true
  }
  this.agentStatus = true
  this.service.getreq('msg')
  this.service.reqs()
    .subscribe((reqs) => {
      this.sid = this.service.socket.id
      console.log(this.sid)
      this.persons = []
      reqs.forEach(element => {
      if ((element.skt === '' || element.skt === this.sid) &&  element.status !== 'none') {
        this.persons.push(element)
      }
      })
      console.log(this.persons)
    })
    this.service.acp()
    .subscribe((acp) => {
      console.log(acp, this.persons)
      console.log(this.persons.findIndex(x => x.id === acp))
    })

    this.service.getMessages()  
    .subscribe((message: string) => {
      console.log('getmessages')    
      console.log(message)
      if (message[0] === localStorage.getItem('id') ) {
      this.addMessage('me', message[1], 'received',new Date().getTime())
      }   
      console.log('message recived', message)
})

}

}

